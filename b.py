
from tracer import trace
import a

@trace
def bb( f ):
   #print 'bb',f
   if f > 0:
     y = a.aa(f-1)
   else:
     y =  0
   return y

@trace
def  f_(i):
  if i > 1:
    result =  a.factorial( i - 1) 
  else:
    result =  1
  return result

if __name__ == '__main__':
   bb (2)
