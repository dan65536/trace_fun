import inspect
import sys


def new_tracer():

  def logging_tracer(frame, event, arg):

    lines, firstline = inspect.getsourcelines(frame)

    args = inspect.getargvalues(frame)
    if event == 'call':
        if args.varargs == 'x_x':
            print '*CALL* with args:', args

    def local_tracer(local_frame, event, arg):
        if event == 'line' and frame is local_frame:
            # dwm
            print "[" + FILE[frame.f_code] + "]",
            print event, frame.f_lineno,'\t', lines[frame.f_lineno - firstline].rstrip()
            #print event, lines[frame.f_lineno - firstline]
            #print frame.f_code.co_name, frame.f_lineno, event, arg

    if frame.f_code in LOG_THESE_FUNCTIONS:
        #print "[" + frame.f_code.co_filename + "]"
        print "[" + FILE[frame.f_code] + "]",
        print event, frame.f_lineno,'\t', lines[frame.f_lineno - firstline + (event == 'call')].rstrip()
        #print frame.f_code.co_name, event, arg
        return local_tracer
    else:
        return None

  return logging_tracer


LOG_THESE_FUNCTIONS = set()
FILE = {}

def trace(func):

    LOG_THESE_FUNCTIONS.add(func.func_code)
    FILE[ func.func_code ] = func.func_code.co_filename

    def _wrapper(*x_x):
        old_trace_function = sys.gettrace()
        #sys.settrace(logging_tracer)
        sys.settrace( new_tracer() )
        try:
            result = func(*x_x)
        except:
            raise
        else:
            return result
        finally:
            sys.settrace(old_trace_function)
    return _wrapper


if __name__ == '__main__':
  @trace
  def f(a,b):
    return a*b

  print  f(3,5)

